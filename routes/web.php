<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/', 'WelcomeController@index');
Route::get('/Cetagory', 'WelcomeController@cetagory');
Route::get('/Contact', 'WelcomeController@contact');
Route::get('/product-details', 'WelcomeController@productDetails');
Auth::routes();
Route::get('/dashboard', 'HomeController@index');

//Srart Category Info
//Route::get('/category-manage', 'CategoryController@manageCategory');
//Route::get('/category-add', 'CategoryController@createCategory');
//Route::post('/category-save', 'CategoryController@storeCategory');
//Route::get('/category/edit/{id}', 'CategoryController@editCategory');
//Route::post('category/update/{id}', 'CategoryController@updateCategory');
//Route::get('category/delete/{id}', 'CategoryController@deleteCategory');
Route::get('admin/category/{id}/delete', 'CategoryController@destroy');
Route::resource('admin/category', 'categoryController');
//End Category Info
//Start Manufacturer Info
Route::get('admin/manufacturer/{id}/delete', 'ManufactureController@destroy');
Route::resource('admin/manufacturer', 'ManufactureController');
//End Manufacturer Info