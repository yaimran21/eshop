<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a style="background-color:black; color:white" ; href="{{ url('/dashboard') }}"><i
                            class="fa fa-dashboard fa-fw"></i> Dashboard <span class="fa arrow"></span></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart fa-fw"></i>Category Info<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ asset('admin/category/create') }}"><i class="fa fa-pencil fa-fw"></i>Add
                            Category<span
                                    class="fa arrow"></span></a>
                    </li>
                    <li>
                        <a href="{{asset('admin/category')}}"><i class="fa fa-book fa-fw"></i>Manage Category<span
                                    class="fa arrow"></a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            <li>
                <a href="#"><i class="fa fa-table fa-fw"></i>Manufacturer Info</a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ asset('admin/manufacturer/create') }}"><i class="fa fa-pencil fa-fw"></i>Add
                            Manufacturer<span class="fa arrow"></span></a>
                    </li>
                    <li>
                        <a href="{{asset('admin/manufacturer')}}"><i class="fa fa-book fa-fw"></i>Manage
                            Manufacturer<span class="fa arrow"></a>
                    </li>

                </ul>
        </ul>
        </li>
    </div>
</div>