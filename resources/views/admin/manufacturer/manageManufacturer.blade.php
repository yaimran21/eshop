@extends('admin.master')
@section('content')

    <div class="col-lg-12">
        <div class="col-lg-offset-3"><h3><b>Update your manufacturer information</b></h3></div>
        <hr/>
        <div class="col-lg-9">
            <table class="table table-bordered" style="margin-left: 250px">
                <tr>
                    <th>ID</th>
                    <th>Manufacturer Name</th>
                    <th>Manufacturer Description</th>
                    <th>Manufacturer Status</th>
                    <th>Action</th>
                </tr>

                @foreach($manufacturers as $manufacturer)
                    <tr>
                        <td>{{$manufacturer->id}}</td>
                        <td>{{$manufacturer->manufacturerName}}</td>
                        <td>{{$manufacturer->manufacturerDescription}}</td>
                        <td>{{$manufacturer->manufacturerSatatus==1?'Published':'Unpublished'}}</td>
                        <td>
                            <a href="{{url('admin/manufacturer',[$manufacturer->id,'edit'])}}" class="btn btn-success">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                            <a href="{{url('admin/manufacturer',[$manufacturer->id,'delete'])}}" class="btn btn-danger">
                                <span class="glyphicon glyphicon-trash"> </span>
                                    {{--{!! Form::open(['url'=>['admin/category',$category->id],'method'=>'DELETE']) !!}--}}
                                    {{--{!! Form::submit() !!}--}}
                                    {{--{!! Form::close() !!}--}}
                            </a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    </div>
@endsection