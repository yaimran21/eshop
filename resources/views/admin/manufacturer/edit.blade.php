@extends('admin.master')
@section('content')
    <div class="container">
        <!-- fieldset collapse start -->
        <div class="cold-lg-12 text-center">
            {{--Show error message--}}
            <h2 class="text-center">Update Manufacturer</h2>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{--Show error message end--}}
            {{--Show session massage--}}
            <h3 class="text-center text-success">{{Session::get('message')}}</h3>
            {{--Show session massage end--}}
            <hr/>
            <div style="margin-left:50px">
                {!!Form::open(['url'=>'admin/manufacturer/'.$manufactureData->id,'method'=>'patch','class'=>'form-horizontal',"name"=>'editForm'])!!}
                <div class="form-group">
                    <label class="control-label col-sm-4" for="name">Manufacture Name:</label>
                    <div class="col-sm-8">
                        <input type="text" name="manufacturerName" class="form-control" id="manufacturerName"
                               placeholder="Enter Category Name" value={{$manufactureData->manufacturerName}}>

                    </div>

                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="manufacturerDescription">Category Description:</label>
                    <div class="col-sm-8">
                    <textarea rows="8" name="manufacturerDescription" class="form-control" id="manufacturerDescription"
                              placeholder="Enter description">{{$manufactureData->manufacturerDescription}}
                    </textarea>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-sm-4" for="Semester">Publication Status:</label>
                    <div class="col-sm-8">
                        <select name="manufacturerSatatus" class="form-control" id="manufacturerSatatus">
                            <option>...Select Status...</option>
                            <option value="1">published</option>
                            <option value="0">Unpublished</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-success btn-block">Save</button>
                    </div>
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>
    <script>
        document.forms['editForm'].elements['manufacturerSatatus'].value ={{$manufactureData->manufacturerSatatus}}
    </script>
@endsection