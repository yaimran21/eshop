@extends('admin.master')
@section('content')

    <div class="col-lg-12">
        <div class="col-lg-offset-3"><h3><b>Update your category information</b></h3></div>
        <hr/>
        <div class="col-lg-9">
            <table class="table table-bordered" style="margin-left: 250px">
                <tr>
                    <th>ID</th>
                    <th>Category Name</th>
                    <th>Category Description</th>
                    <th>Publication Status</th>
                    <th>Action</th>
                </tr>

                @foreach($categories as $category)
                    <tr>
                        <td>{{$category->id}}</td>
                        <td>{{$category->categoryName}}</td>
                        <td>{{$category->categoryDescription}}</td>
                        <td>{{$category->publicationSatatus==1?'Published':'Unpublished'}}</td>
                        <td>
                            <a href="{{url('admin/category',[$category->id,'edit'])}}" class="btn btn-success">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                            <a href="{{url('admin/category',[$category->id,'delete'])}}" class="btn btn-danger">
                                <span class="glyphicon glyphicon-trash"> </span>
                                    {{--{!! Form::open(['url'=>['admin/category',$category->id],'method'=>'DELETE']) !!}--}}
                                    {{--{!! Form::submit() !!}--}}
                                    {{--{!! Form::close() !!}--}}

                            </a>

                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    </div>
@endsection