@extends('admin.master')
@section('content')
    <div class="container">
        <!-- fieldset collapse start -->
        <div class="cold-lg-12 text-center">
            {{--Show error message--}}
            <h2 class="text-center">Save Category</h2>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{--Show error message end--}}
            {{--Show session massage--}}
            <h3 class="text-center text-success">{{Session::get('message')}}</h3>
            {{--Show session massage end--}}
            <hr/>
            {!!Form::open(['url'=>'admin/category','method'=>'POST','class'=>'form-horizontal'])!!}

            <div class="form-group">
                <label class="control-label col-sm-4" for="name">Category Name:</label>
                <div class="col-sm-8">
                    <input type="text" name="categoryName" class="form-control" id="name"
                           placeholder="Enter Category Name">
                </div>

            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="description">Category Description:</label>
                <div class="col-sm-8">
                    <textarea rows="8" name="categoryDescription" class="form-control" id="description"
                              placeholder="Enter description"></textarea>
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-sm-4" for="Semester">Publication Status:</label>
                <div class="col-sm-8">
                    <select name="publicationSatatus" class="form-control" id="Semester">
                        <option>...Select Status...</option>
                        <option value="1">published</option>
                        <option value="0">Unpublished</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-success btn-block">Save</button>
                </div>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
@endsection