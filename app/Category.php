<?php
namespace Request;
use Illuminate\Database\Eloquent\Model;
class Category extends Model {
	protected $fillable = ['categoryName', 'categoryDescription', 'publicationSatatus'];
}
