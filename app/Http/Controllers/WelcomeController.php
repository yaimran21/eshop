<?php

namespace Request\Http\Controllers;

class WelcomeController extends Controller {
	public function index() {
		return view('frontEnd.home.homeContent');
	}
	public function cetagory() {
		return view("frontEnd.cetagory.cetagoryContent");
	}
	public function contact() {
		return view("frontEnd.contact.contactContent");
	}
	public function productDetails() {
		return view("frontEnd.product.productDetails");
	}

}
