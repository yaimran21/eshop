<?php
namespace Request\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Request\Category;
use Request\User;

class CategoryController extends Controller
{
    public function create()
    {
        return view('admin.category.createCategory');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'categoryName' => 'required|min:3|max:20',
            'categoryDescription' => 'required|min:20|max:200',
            'publicationSatatus' => 'required'
        ]);
        $data = $request->all();
        Category::create($data);
        if ($data) {
            return redirect()->back()->with('message', 'Category Save Successfully');
        }
    }

    public function index()
    {
        $categories = Category::all();
        return view('admin.category.manageCategory', ['categories' => $categories]);
    }

    public function edit($id)
    {
        $categoryData = Category::find($id);
        return view('admin.category.edit', compact('categoryData'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'categoryName' => 'required|min:3|max:20',
            'categoryDescription' => 'required|min:20',
            'publicationSatatus' => 'required'
        ]);
        $data = $request->all();
        $existingData = Category::find($id);
        $existingData->update($data);
        if ($data) {
         Session::flash('message','Category Update Successfully');
            return back();
        }
    }
    public function destroy($id){

        $existingData = Category::find($id);
        $existingData->delete();
            Session::flash('message','Category Delete Successfully');
            return back();

    }
}