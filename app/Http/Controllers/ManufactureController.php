<?php
namespace Request\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Request\Manufacture;
use Request\User;

class ManufactureController extends Controller
{
    public function index()
    {
        $manufacturers = Manufacture::all();
        return view('admin.manufacturer.manageManufacturer', ['manufacturers' => $manufacturers]);
    }
    public function create()
    {
        return view('admin.manufacturer.createManufacturer');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'manufacturerName' => 'required|min:3|max:20',
            'manufacturerDescription' => 'required|min:20|max:200',
            'manufacturerSatatus' => 'required'
        ]);
        $data = $request->all();
        Manufacture::create($data);
        if ($data) {
            return redirect()->back()->with('message', 'Manufacture Save Successfully');
        }
    }
    public function edit($id)
    {
        $manufactureData = Manufacture::find($id);
        return view('admin.manufacturer.edit', compact('manufactureData'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'manufacturerName' => 'required|min:3|max:20',
            'manufacturerDescription' => 'required|min:20',
            'manufacturerSatatus' => 'required'
        ]);
        $data = $request->all();
        $existingData = Manufacture::find($id);
        $existingData->update($data);
        if ($data) {
            Session::flash('message','Category Update Successfully');
            return back();
        }
    }
    public function destroy($id){

        $existingData = Manufacture::find($id);
        $existingData->delete();
        Session::flash('message','Category Delete Successfully');
        return back();

    }

}
