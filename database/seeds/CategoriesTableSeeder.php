<?php
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            $data = [
                'categoryName' => str_random(10),
                'categoryDescription' => str_random(50),
                'publicationSatatus' => str_random(1),
            ];
            DB::table('categories')->insert($data);
        }
    }
}
